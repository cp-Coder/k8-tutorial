# K8 Tutorial

This repo contains kubernetes configuration files through which I practiced my kubernetes skills

## Tools that I used

- VSCode
- YAML Extension by Red Hat (Great tool for validating kubernetes configuration files)
- Docker for container runtime
- Kubectl for managing cluster
- Minikube for practising and testing kubernetes locally

## Steps to create kubernetes cluster

### Follow these steps to practise kubernetes on cloud

1. We will create a cluster of __1 master node and 2 worker nodes__. Create one small instance (usually 2 CPU and 2GB RAM) which will be the master node as master node don't have high workload. Now create 2 medium instances (usually 2 CPU and 4GB RAM) which will be the worker nodes with ubuntu as OS in all the three instances.

2. Now, we have to install `container runtime` to schedule containers, `kube proxy` and `kubelet` application on all the instances. Additionally, on master node, we have to install other kubernetes control panels applications like `API server`, `Scheduler`, `Controller Manager`, and `etcd` as pods. Also, self-signed `TLS Certificate` to validate request in the kubernetes cluster. These configuration and process will be managed by `kubeadm`, tool maintained by Kubernetes

3. Before that, disable swap __permanently__ on each instances because K8s scheduler determines the best available node on which to deploy newly created pods. If memory swapping is allowed to occur on host system, this can lead to performance and stability issues with Kubernetes. Also open some ports for kubernetes to communicate properly. Check the details [here](https://kubernetes.io/docs/reference/networking/ports-and-protocols/) to see which port should be open.

    ```bash
      # to disable swap temporarily
      sudo swapoff -a

      # to disable permanently
      sudo sed -i '/ swap / s/^/#/' /etc/fstab 
    ```

4. Install `containerd` as container runtime from [here](https://github.com/containerd/containerd/blob/main/docs/getting-started.md). After finish the setup, install `kubectl`, `kubelet` and `kubeadm` by running the following commands:

    ```bash
      # update apt package and install necessary tools
      sudo apt-get update && sudo apt-get install -y apt-transport-https ca-certificates curl

      # download the public key and add kubernetes in apt repository
      curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-archive-keyring.gpg && echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list

      # now install the packages
      sudo apt-get update && sudo apt-get install -y kubelet kubeadm kubectl && sudo apt-mark hold kubelet kubeadm kubectl
    ```

    Make sure to install all three packages on both the worker nodes and master node.

5. Now run the following command to prepare necessary certificates and configurations manifest to create and deploy pods of all the necessary components of control panel

    ```bash
      sudo kubeadm init
      # does the following things:
      # preflight                    Run pre-flight checks
      # certs                        Certificate generation
      #   /ca                          Generate the self-signed Kubernetes CA to provision identities for other Kubernetes components
      #   /apiserver                   Generate the certificate for serving the Kubernetes API
      #   /apiserver-kubelet-client    Generate the certificate for the API server to connect to kubelet
      #   /front-proxy-ca              Generate the self-signed CA to provision identities for front proxy
      #   /front-proxy-client          Generate the certificate for the front proxy client
      #   /etcd-ca                     Generate the self-signed CA to provision identities for etcd
      #   /etcd-server                 Generate the certificate for serving etcd
      #   /etcd-peer                   Generate the certificate for etcd nodes to communicate with each other
      #   /etcd-healthcheck-client     Generate the certificate for liveness probes to healthcheck etcd
      #   /apiserver-etcd-client       Generate the certificate the apiserver uses to access etcd
      #   /sa                          Generate a private key for signing service account tokens along with its public key
      # kubeconfig                   Generate all kubeconfig files necessary to establish the control plane and the admin kubeconfig file
      #   /admin                       Generate a kubeconfig file for the admin to use and for kubeadm itself
      #   /kubelet                     Generate a kubeconfig file for the kubelet to use *only* for cluster bootstrapping purposes
      #   /controller-manager          Generate a kubeconfig file for the controller manager to use
      #   /scheduler                   Generate a kubeconfig file for the scheduler to use
      # kubelet-start                Write kubelet settings and (re)start the kubelet
      # control-plane                Generate all static Pod manifest files necessary to establish the control plane
      #   /apiserver                   Generates the kube-apiserver static Pod manifest
      #   /controller-manager          Generates the kube-controller-manager static Pod manifest
      #   /scheduler                   Generates the kube-scheduler static Pod manifest
      # etcd                         Generate static Pod manifest file for local etcd
      #   /local                       Generate the static Pod manifest file for a local, single-node local etcd instance
      # upload-config                Upload the kubeadm and kubelet configuration to a ConfigMap
      #   /kubeadm                     Upload the kubeadm ClusterConfiguration to a ConfigMap
      #   /kubelet                     Upload the kubelet component config to a ConfigMap
      # upload-certs                 Upload certificates to kubeadm-certs
      # mark-control-plane           Mark a node as a control-plane
      # bootstrap-token              Generates bootstrap tokens used to join a node to a cluster
      # kubelet-finalize             Updates settings relevant to the kubelet after TLS bootstrap
      #   /experimental-cert-rotation  Enable kubelet client certificate rotation
      # addon                        Install required addons for passing conformance tests
      #   /coredns                     Install the CoreDNS addon to a Kubernetes cluster
      #   /kube-proxy                  Install the kube-proxy addon to a Kubernetes cluster
      # show-join-command            Show the join command for control-plane and worker node
    ```

6. Now run the following command to authenticate via kubeconfig file (file having API server address, and client certificates & private key) using kubectl

    ```bash
      # to get information about nodes
      kubectl get nodes
      # will give error saying connection is refused as it don't know the address of API server
      # so pass kubeconfig file with above command
      sudo kubectl get nodes --kubeconfig /etc/kubernetes/admin.conf
    ```

    __Note__: Copy the `admin.conf` file to ~/.kube as `config` (create if not exists) to avoid using sudo again and again. Also change the permission of that file

    ```bash
      # create the directory
      mkdir -p ~/.kube
      # copy the file
      sudo cp -i /etc/kubernetes/admin.conf ~/.kube/config
      # change permission (using Ubuntu)
      sudo chown $(id -u):$(id -g) ~/.kube/config
    ```

### Follow these steps to practise kubernetes using minikube

1. Install `minikube` tool for respective operating system.

2. Now start the local kubernetes cluster from the following command:

    ```bash
      # For container-runtime, valid options: docker, cri-o, containerd (default: auto)
      # using cilium for CNI (container networking interface)
      minikube start --container-runtime=containerd --cni=cilium
      # for windows, specify --driver flag to select the driver for running K8 cluster
    ```
